-- MySQL dump 10.13  Distrib 8.0.32, for Linux (x86_64)
--
-- Host: localhost    Database: devcamp_album
-- ------------------------------------------------------
-- Server version	8.0.32-0ubuntu0.22.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `album`
--

DROP TABLE IF EXISTS `album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `album` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_8ikoikfo2jrctonyt3cfs2lsd` (`code`),
  UNIQUE KEY `UC_album_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `album`
--

LOCK TABLES `album` WRITE;
/*!40000 ALTER TABLE `album` DISABLE KEYS */;
INSERT INTO `album` VALUES (1,'KIM','2023-04-30 15:05:20','Thuộc tính Kim','Album những vật Hệ Kim'),(2,'MỘC','2023-04-30 15:09:22','Thuộc tính Mộc','Album những vật Hệ Mộc'),(3,'THỦY','2023-04-30 15:11:11','Thuộc tính Thủy','Album những vật Hệ Thủy'),(4,'HỎA','2023-04-30 15:12:46','Thuộc tính Hỏa','Album những vật Hệ Hỏa'),(5,'THỔ','2023-04-30 15:14:52','Thuộc tính Thổ','Album những vật Hệ Thổ'),(6,'taolao','2023-05-02 03:03:38','Ngũ Hành','Ngũ Hành Chi Vât, Chi Nhân');
/*!40000 ALTER TABLE `album` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photo`
--

DROP TABLE IF EXISTS `photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `photo` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `album_id` bigint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_r32jk9odsrk1wgiir87e1o4u9` (`code`),
  KEY `FKpy64km2p72eoy5rwh31ria0vx` (`album_id`),
  CONSTRAINT `FKpy64km2p72eoy5rwh31ria0vx` FOREIGN KEY (`album_id`) REFERENCES `album` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photo`
--

LOCK TABLES `photo` WRITE;
/*!40000 ALTER TABLE `photo` DISABLE KEYS */;
INSERT INTO `photo` VALUES (1,'kim1','2023-04-30 18:10:39','kimphoto1','Hình Hệ Kim','images/kim1.jpeg',1),(2,'kim2','2023-04-30 18:12:37','kimphoto2','Hình Hệ Kim','images/kim2.jpeg',1),(3,'moc1','2023-04-30 18:14:58','mocphoto1','Hình Hệ Mộc','images/moc1.jpeg',2),(4,'moc2','2023-04-30 18:15:44','mocphoto2','Hình Hệ Mộc','images/moc2.jpeg',2),(5,'thuy1','2023-04-30 18:17:39','thuyphoto1','Hình Hệ Thủy','images/thuy1.jpeg',3),(6,'thuy2','2023-04-30 18:19:11','thuyphoto2','Hình Hệ Thủy','images/thuy2.jpeg',3),(7,'hoa1','2023-04-30 18:21:09','hoaphoto1','Hình Hệ Hỏa','images/hoa1.jpeg',4),(8,'hoa2','2023-04-30 18:23:33','hoaphoto2','Hình Hệ Hỏa','images/hoa2.jpeg',4),(9,'tho1','2023-04-30 18:24:39','thophoto1','Hình Hệ Thổ','images/tho1.jpeg',5),(10,'kim3','2023-04-30 18:30:21','thophoto2','Hình Hệ Thổ','images/tho2.jpeg',5);
/*!40000 ALTER TABLE `photo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-05-02 20:10:33

package com.devcamp.albumphotocrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.albumphotocrud.model.CAlbum;

public interface IAlbumRepository extends JpaRepository<CAlbum, Long> {
    CAlbum findById(long id);
}

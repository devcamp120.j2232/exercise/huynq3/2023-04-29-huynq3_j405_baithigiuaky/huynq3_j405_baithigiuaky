package com.devcamp.albumphotocrud.model;

import java.util.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table (name = "photo")
public class CPhoto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty(message = "Nhập mã ảnh")
    @Column (name = "code", unique = true)
    private String code;

    @NotEmpty(message = "Nhập tên ảnh")
    @Column (name = "name")
    private String name;
    
    @NotEmpty(message = "Nhập đường link ảnh")
    @Column (name = "url")
    private String url;

    @NotEmpty(message = "Nhập mô tả ảnh")
    @Column (name = "title")
    private String title;

    @Temporal(TemporalType.TIMESTAMP)
    @Column (name = "created", nullable = true, updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date created;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "album_id", nullable = false)
    @JsonIgnore
    private CAlbum album;
    
    public CPhoto() {
    }
    
    public CPhoto(long id, String name, String code, String url, String title, Date created, CAlbum album) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.url = url;
        this.title = title;
        this.created = created;
        this.album = album;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public CAlbum getAlbum() {
        return album;
    }

    public void setAlbum(CAlbum album) {
        this.album = album;
    }

    
}

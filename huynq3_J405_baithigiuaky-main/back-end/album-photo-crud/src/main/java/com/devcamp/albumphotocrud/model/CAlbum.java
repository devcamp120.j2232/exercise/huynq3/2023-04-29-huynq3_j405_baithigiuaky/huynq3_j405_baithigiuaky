package com.devcamp.albumphotocrud.model;

import java.util.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table (name = "album")
public class CAlbum {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @NotEmpty(message = "Nhập mã album")
    @Column (name = "code", unique = true)
    private String code;

    @NotEmpty(message = "Nhập tên album")
    @Column (name = "name")
    private String name;

    @NotEmpty(message = "Nhập mô tả album")
    @Column (name = "title")
    private String title;

    @Temporal(TemporalType.TIMESTAMP)
    @Column (name = "created", nullable = true, updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date created;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "album")
    private List<CPhoto> photos;
    
    public CAlbum() {
    }

    public CAlbum(long id, String code, String name, String title, Date created, List<CPhoto> photos) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.title = title;
        this.created = created;
        this.photos = photos;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<CPhoto> getPhotos() {
        return photos;
    }

    public void setPhotos(List<CPhoto> photos) {
        this.photos = photos;
    }

    
    
}
